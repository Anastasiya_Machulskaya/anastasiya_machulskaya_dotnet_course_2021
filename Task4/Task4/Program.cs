﻿using System;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите трехзначное число: ");
            int number = Convert.ToInt32(Console.ReadLine());
            int[] argument = new int[3];
            for (int i = 0; i < 3; i++)
            {
                argument[i] = number % 10;
                number = (number - argument[i]) / 10;
            }
            if (argument[0] > argument[1] && argument[0] > argument[2])
            {
                Console.WriteLine(argument[0]);
            }
            else if (argument[1] > argument[2])
            {
                Console.WriteLine(argument[1]);
            }
            else
            {
                Console.WriteLine(argument[2]);
            }
        }
    }
}
