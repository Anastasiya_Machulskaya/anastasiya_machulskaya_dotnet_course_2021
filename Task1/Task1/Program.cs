﻿using System;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Пусть даны два числа. Первое равно 8, а второе 32");
            int number1 = 8;
            int number2 = 32;
            int helpingNumber = 0;
            Console.WriteLine("Давайте поменяем эти числа местами");
            helpingNumber = number1;
            number1 = number2;
            number2 = helpingNumber;
            Console.WriteLine($"Теперь первое число равно {number1}, а второе число равно {number2}");
        }
    }
}
